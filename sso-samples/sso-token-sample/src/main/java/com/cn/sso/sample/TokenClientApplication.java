package com.cn.sso.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TokenClientApplication {

	public static void main(String[] args) {
        SpringApplication.run(TokenClientApplication.class, args);
	}

}