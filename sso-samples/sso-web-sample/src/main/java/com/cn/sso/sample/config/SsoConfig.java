package com.cn.sso.sample.config;

import com.cn.sso.core.conf.Conf;
import com.cn.sso.core.filter.SsoWebFilter;
import com.cn.sso.core.util.JedisUtil;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SsoConfig implements DisposableBean {


    @Value("${micro.sso.server}")
    private String microSsoServer;

    @Value("${micro.sso.logout.path}")
    private String microSsoLogoutPath;

    @Value("${micro-sso.excluded.paths}")
    private String microSsoExcludedPaths;

    @Value("${micro.sso.redis.address}")
    private String microSsoRedisAddress;


    @Bean
    public FilterRegistrationBean microSsoFilterRegistration() {

        // micro-sso, redis init
        JedisUtil.init(microSsoRedisAddress);

        // micro-sso, filter init
        FilterRegistrationBean registration = new FilterRegistrationBean();

        registration.setName("MicroSsoWebFilter");
        registration.setOrder(1);
        registration.addUrlPatterns("/*");
        registration.setFilter(new SsoWebFilter());
        registration.addInitParameter(Conf.SSO_SERVER, microSsoServer);
        registration.addInitParameter(Conf.SSO_LOGOUT_PATH, microSsoLogoutPath);
        registration.addInitParameter(Conf.SSO_EXCLUDED_PATHS, microSsoExcludedPaths);

        return registration;
    }

    @Override
    public void destroy() throws Exception {

        // micro-sso, redis close
        JedisUtil.close();
    }

}
