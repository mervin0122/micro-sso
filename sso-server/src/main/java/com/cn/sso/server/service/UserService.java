package com.cn.sso.server.service;

import com.cn.sso.server.core.model.UserInfo;
import com.cn.sso.server.core.result.ReturnT;

public interface UserService {

    public ReturnT<UserInfo> findUser(String username, String password);

}
